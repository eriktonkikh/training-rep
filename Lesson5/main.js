const calcKeysHandlers = (function () {
  const initialOutput = 0;
  const calculator = new Calculator(initialOutput);
  const calculationOutput = document.getElementById("calculation-result");
  calculationOutput.innerText = initialOutput;

  let intendOperation = (resolveRepeatOperation = function () {
    return null;
  });

  function inverseLOperand() {
    currentOperand.significant *= -1n;
    const lOperand =
      Number(currentOperand.significant) *
      10 ** Number(currentOperand.exponent);

    calculator.setLOperand(lOperand);

    const significantStr = currentOperand.significant.toString();
    if (isFloat) {
      const exponentNumber = Number(currentOperand.exponent);
      calculationOutput.innerText =
        significantStr.substring(0, significantStr.length + exponentNumber) +
        "." +
        significantStr.substring(
          significantStr.length + exponentNumber,
          significantStr.length
        );
      if (!(currentOperand.significant < 0))
        significantStr.length === -exponentNumber &&
          (calculationOutput.innerText = "0" + calculationOutput.innerText);
      else
        Math.abs(Number(currentOperand.significant)).toString().length ===
          -exponentNumber &&
          (calculationOutput.innerText =
            calculationOutput.innerText.substring(0, 1) +
            "0" +
            calculationOutput.innerText.substring(
              1,
              calculationOutput.innerText.length
            ));
    } else calculationOutput.innerText = significantStr;
  }

  function inverseROperand() {
    currentOperand.significant *= -1n;
    const rOperand =
      Number(currentOperand.significant) *
      10 ** Number(currentOperand.exponent);

    calculator.setROperand(rOperand);

    const significantStr = currentOperand.significant.toString();
    if (isFloat) {
      const exponentNumber = Number(currentOperand.exponent);
      calculationOutput.innerText =
        significantStr.substring(0, significantStr.length + exponentNumber) +
        "." +
        significantStr.substring(
          significantStr.length + exponentNumber,
          significantStr.length
        );
      if (!(currentOperand.significant < 0))
        significantStr.length === -exponentNumber &&
          (calculationOutput.innerText = "0" + calculationOutput.innerText);
      else
        Math.abs(Number(currentOperand.significant)).toString().length ===
          -exponentNumber &&
          (calculationOutput.innerText =
            calculationOutput.innerText.substring(0, 1) +
            "0" +
            calculationOutput.innerText.substring(
              1,
              calculationOutput.innerText.length
            ));
    } else calculationOutput.innerText = significantStr;
  }

  let inverseOperand = function () {
    if (currentOperandType === "l") inverseLOperand();
    else if (currentOperandType === "r") inverseROperand();
  };

  let isFloat = false;
  let currentOperand = {
    significant: 0n,
    exponent: 0n,
  };

  function inputLOperand(value) {
    if (currentOperand.exponent <= -8n) return;

    currentOperand.significant *= 10n;
    currentOperand.significant += BigInt(value);

    isFloat && currentOperand.exponent--;

    const lOperand =
      Number(currentOperand.significant) *
      10 ** Number(currentOperand.exponent);

    calculator.setLOperand(lOperand);
    calculator.setROperand(lOperand);

    const significantStr = currentOperand.significant.toString();
    if (isFloat) {
      const exponentNumber = Number(currentOperand.exponent);
      calculationOutput.innerText =
        significantStr.substring(0, significantStr.length + exponentNumber) +
        "." +
        significantStr.substring(
          significantStr.length + exponentNumber,
          significantStr.length
        );
      significantStr.length === -exponentNumber &&
        (calculationOutput.innerText = "0" + calculationOutput.innerText);
    } else calculationOutput.innerText = significantStr;
  }

  function inputROperand(value) {
    if (doesClearROperand) {
      calculator.clearROperand();
      currentOperand = {
        significant: 0n,
        exponent: 0n,
      };
      isFloat = false;
      calculationOutput.innerText = 0;
    }
    doesClearROperand = false;

    if (currentOperand.exponent <= -8n) return;

    currentOperand.significant *= 10n;
    currentOperand.significant += BigInt(value);

    isFloat && currentOperand.exponent--;

    const rOperand =
      Number(currentOperand.significant) *
      10 ** Number(currentOperand.exponent);

    calculator.setROperand(rOperand);

    const significantStr = currentOperand.significant.toString();
    if (isFloat) {
      const exponentNumber = Number(currentOperand.exponent);
      calculationOutput.innerText =
        significantStr.substring(0, significantStr.length + exponentNumber) +
        "." +
        significantStr.substring(
          significantStr.length + exponentNumber,
          significantStr.length
        );
      significantStr.length === -exponentNumber &&
        (calculationOutput.innerText = "0" + calculationOutput.innerText);
    } else calculationOutput.innerText = significantStr;
  }

  function backspace() {
    doesClearROperand = false;
    if (currentOperand.exponent >= 0n && isFloat === true) {
      isFloat = false;
      const significantStr = currentOperand.significant.toString();
      calculationOutput.innerText = significantStr;
      return;
    }

    if (currentOperandType === "l") {
      currentOperand.significant = currentOperand.significant / 10n;
      currentOperand.exponent < 0n && currentOperand.exponent++;

      const lOperand =
        Number(currentOperand.significant) *
        10 ** Number(currentOperand.exponent);

      calculator.setLOperand(lOperand);
      calculator.setROperand(lOperand);

      const significantStr = currentOperand.significant.toString();
      if (isFloat) {
        const exponentNumber = Number(currentOperand.exponent);
        calculationOutput.innerText =
          significantStr.substring(0, significantStr.length + exponentNumber) +
          "." +
          significantStr.substring(
            significantStr.length + exponentNumber,
            significantStr.length
          );
      } else calculationOutput.innerText = significantStr;
    } else if (currentOperandType === "r") {
      currentOperand.significant = currentOperand.significant / 10n;
      currentOperand.exponent < 0n && currentOperand.exponent++;
      currentOperand.exponent >= 0n && (isFloat = false);

      const rOperand =
        Number(currentOperand.significant) *
        10 ** Number(currentOperand.exponent);

      calculator.setROperand(rOperand);

      const significantStr = currentOperand.significant.toString();
      if (isFloat) {
        const exponentNumber = Number(currentOperand.exponent);
        calculationOutput.innerText =
          significantStr.substring(0, significantStr.length + exponentNumber) +
          "." +
          significantStr.substring(
            significantStr.length + exponentNumber,
            significantStr.length
          );
      } else calculationOutput.innerText = significantStr;
    }
  }

  let doesClearROperand = false;

  let lastOperation = null;

  let currentOperandType = "l";

  function inputOperand(value) {
    if (currentOperandType === "l") inputLOperand(value);
    else if (currentOperandType === "r") inputROperand(value);
  }

  function add1() {
    inputOperand(1);
  }
  function add2() {
    inputOperand(2);
  }
  function add3() {
    inputOperand(3);
  }
  function add4() {
    inputOperand(4);
  }
  function add5() {
    inputOperand(5);
  }
  function add6() {
    inputOperand(6);
  }
  function add7() {
    inputOperand(7);
  }
  function add8() {
    inputOperand(8);
  }
  function add9() {
    inputOperand(9);
  }
  function add0() {
    inputOperand(0);
  }

  function addDot() {
    currentOperand.exponent === 0n && (isFloat = !isFloat);
    calculationOutput.innerText += ".";
  }

  function clearButtonPressedClasses() {
    document.getElementById("sum")?.classList.remove("pressed");
    document.getElementById("sub")?.classList.remove("pressed");
    document.getElementById("mul")?.classList.remove("pressed");
    document.getElementById("div")?.classList.remove("pressed");
  }

  function sum() {
    if (lastOperation !== "sum")
      calculator.setROperand(calculator.getLOperand());
    else {
      const result = intendOperation();
      if (typeof result === "number" && !isNaN(result))
        calculationOutput.innerText = 1 * result.toFixed(8);
    }
    lastOperation = "sum";
    new Promise((resolve) => {
      clearButtonPressedClasses();
      setTimeout(() => resolve(), 50);
    }).then(() => document.getElementById("sum")?.classList.add("pressed"));

    intendOperation = resolveRepeatOperation = function () {
      let rOperand = calculator.getROperand();
      if (rOperand === undefined || rOperand === null) {
        rOperand = calculator.getLOperand();
        calculator.setROperand(rOperand);
      }
      const result = Math.round(calculator.sum() * 10 ** 8) / 10 ** 8;
      if (result === 0) {
        currentOperand.significant = 0n;
        currentOperand.exponent = 0n;
        return result;
      }
      currentOperand.significant = BigInt(Math.trunc(result * 10 ** 8));
      currentOperand.exponent = BigInt(-8);
      while (currentOperand.significant % 10n === 0n) {
        currentOperand.significant = currentOperand.significant / 10n;
        currentOperand.exponent++;
      }
      isFloat = false;
      if (currentOperand.exponent < 0n) isFloat = true;
      return result;
    };

    doesClearROperand = true;
    currentOperandType = "r";
  }

  function mul() {
    if (lastOperation !== "mul")
      calculator.setROperand(calculator.getLOperand());
    else {
      const result = intendOperation();
      if (typeof result === "number" && !isNaN(result))
        calculationOutput.innerText = 1 * result.toFixed(8);
    }
    lastOperation = "mul";
    new Promise((resolve) => {
      clearButtonPressedClasses();
      setTimeout(() => resolve(), 50);
    }).then(() => document.getElementById("mul")?.classList.add("pressed"));

    intendOperation = resolveRepeatOperation = function () {
      let rOperand = calculator.getROperand();
      if (rOperand === undefined || rOperand === null) {
        rOperand = calculator.getLOperand();
        calculator.setROperand(rOperand);
      }
      const result = Math.round(calculator.mul() * 10 ** 8) / 10 ** 8;
      if (result === 0) {
        currentOperand.significant = 0n;
        currentOperand.exponent = 0n;
        return result;
      }
      currentOperand.significant = BigInt(Math.trunc(result * 10 ** 8));
      currentOperand.exponent = BigInt(-8);
      while (currentOperand.significant % 10n === 0n) {
        currentOperand.significant = currentOperand.significant / 10n;
        currentOperand.exponent++;
      }
      isFloat = false;
      if (currentOperand.exponent < 0n) isFloat = true;
      return result;
    };

    doesClearROperand = true;
    currentOperandType = "r";
  }

  function sub() {
    if (lastOperation !== "sub")
      calculator.setROperand(calculator.getLOperand());
    else {
      const result = intendOperation();
      if (typeof result === "number" && !isNaN(result))
        calculationOutput.innerText = 1 * result.toFixed(8);
    }
    lastOperation = "sub";
    new Promise((resolve) => {
      clearButtonPressedClasses();
      setTimeout(() => resolve(), 50);
    }).then(() => document.getElementById("sub")?.classList.add("pressed"));

    intendOperation = resolveRepeatOperation = function () {
      let rOperand = calculator.getROperand();
      if (rOperand === undefined || rOperand === null) {
        rOperand = calculator.getLOperand();
        calculator.setROperand(rOperand);
      }
      const result = Math.round(calculator.sub() * 10 ** 8) / 10 ** 8;
      if (result === 0) {
        currentOperand.significant = 0n;
        currentOperand.exponent = 0n;
        return result;
      }
      currentOperand.significant = BigInt(Math.trunc(result * 10 ** 8));
      currentOperand.exponent = BigInt(-8);
      while (currentOperand.significant % 10n === 0n) {
        currentOperand.significant = currentOperand.significant / 10n;
        currentOperand.exponent++;
      }
      isFloat = false;
      if (currentOperand.exponent < 0n) isFloat = true;
      return result;
    };

    doesClearROperand = true;
    currentOperandType = "r";
  }

  function div() {
    if (lastOperation !== "div")
      calculator.setROperand(calculator.getLOperand());
    else {
      let result;
      try {
        result = intendOperation();
      } catch (e) {
        reset();
        calculationOutput.innerText = "Err";
        return;
      }
      if (typeof result === "number" && !isNaN(result))
        calculationOutput.innerText = 1 * result.toFixed(8);
    }
    lastOperation = "div";
    new Promise((resolve) => {
      clearButtonPressedClasses();
      setTimeout(() => resolve(), 50);
    }).then(() => document.getElementById("div")?.classList.add("pressed"));

    intendOperation = resolveRepeatOperation = function () {
      let rOperand = calculator.getROperand();
      if (rOperand === undefined || rOperand === null) {
        rOperand = calculator.getLOperand();
        calculator.setROperand(rOperand);
      }
      const result = Math.round(calculator.div() * 10 ** 8) / 10 ** 8;
      if (result === 0) {
        currentOperand.significant = 0n;
        currentOperand.exponent = 0n;
        return result;
      }
      currentOperand.significant = BigInt(Math.trunc(result * 10 ** 8));
      currentOperand.exponent = BigInt(-8);
      while (currentOperand.significant % 10n === 0n) {
        currentOperand.significant = currentOperand.significant / 10n;
        currentOperand.exponent++;
      }
      isFloat = false;
      if (currentOperand.exponent < 0n) isFloat = true;
      return result;
    };

    doesClearROperand = true;
    currentOperandType = "r";
  }

  function inverseSign() {
    inverseOperand();
  }

  function reset() {
    calculator.reset();
    currentOperand = {
      significant: 0n,
      exponent: 0n,
    };
    calculationOutput.innerText = initialOutput;

    intendOperation = resolveRepeatOperation = function () {};
    lastOperation = null;
    currentOperandType = "l";
    clearButtonPressedClasses();
    isFloat = false;
  }

  function resolveOperation() {
    let result;
    try {
      result = resolveRepeatOperation();
    } catch (e) {
      reset();
      calculationOutput.innerText = "Err";
      return;
    }
    if (result === null || result === undefined) return;
    intendOperation = function () {};

    calculationOutput.innerText = 1 * result.toFixed(8);

    doesClearROperand = true;

    lastOperation = null;
    currentOperandType = "r";
    clearButtonPressedClasses();
  }

  return {
    add1,
    add2,
    add3,
    add4,
    add5,
    add6,
    add7,
    add8,
    add9,
    add0,
    backspace,
    addDot,
    sum,
    mul,
    sub,
    div,
    inverseSign,
    reset,
    resolveOperation,
  };
})();
