class Calculator {
  #lOperand;
  #rOperand;

  constructor(initialValue = 0) {
    if (typeof initialValue === "string")
      initialValue = parseFloat(initialValue);

    if (!this.#isValidNumber(initialValue))
      throw new Error("initialValue is not valid");

    this.#lOperand = initialValue;

    this.clearROperand = this.clearROperand.bind(this);
    this.clearLOperand = this.clearLOperand.bind(this);
    this.reset = this.reset.bind(this);
    this.getResult = this.getResult.bind(this);
    this.setLOperand = this.setLOperand.bind(this);
    this.setROperand = this.setROperand.bind(this);
    this.getLOperand = this.getLOperand.bind(this);
    this.getROperand = this.getROperand.bind(this);
    this.sum = this.sum.bind(this);
    this.mul = this.mul.bind(this);
    this.sub = this.sub.bind(this);
    this.div = this.div.bind(this);
  }

  #isValidNumber(v) {
    if (typeof v === "number" && !isNaN(v) && v !== Infinity && v !== -Infinity)
      return true;
    return false;
  }

  clearROperand() {
    this.#rOperand = null;
  }

  clearLOperand() {
    this.#lOperand = null;
  }

  reset(initialValue = 0) {
    if (typeof initialValue === "string")
      initialValue = parseFloat(initialValue);

    if (!this.#isValidNumber(initialValue))
      throw new Error("initialValue is not valid");

    this.#lOperand = initialValue;

    this.#rOperand = undefined;
  }

  getResult() {
    return this.#lOperand;
  }

  getLOperand() {
    return this.#lOperand;
  }

  getROperand() {
    return this.#rOperand;
  }

  setLOperand(lOperand) {
    lOperand = typeof lOperand === "string" ? parseFloat(lOperand) : lOperand;
    if (!this.#isValidNumber(lOperand))
      throw new Error("lOperand is not valid");

    return (this.#lOperand = lOperand);
  }

  setROperand(rOperand) {
    rOperand = typeof rOperand === "string" ? parseFloat(rOperand) : rOperand;
    if (!this.#isValidNumber(rOperand))
      throw new Error("lOperand is not valid");

    return (this.#rOperand = rOperand);
  }

  sum(value) {
    let rOperand;
    if (value === undefined) {
      rOperand = this.#rOperand;
    } else {
      value = typeof value === "string" ? parseFloat(value) : value;
      if (!this.#isValidNumber(value)) throw new Error("value is not valid");
      rOperand = value;
    }

    rOperand = rOperand ?? this.#lOperand;

    const result = this.#lOperand + rOperand;
    this.#lOperand = result;
    return result;
  }

  mul(value) {
    let rOperand;
    if (value === undefined) {
      rOperand = this.#rOperand;
    } else {
      value = typeof value === "string" ? parseFloat(value) : value;
      if (!this.#isValidNumber(value)) throw new Error("value is not valid");
      rOperand = value;
    }

    rOperand = rOperand ?? this.#lOperand;

    const result = this.#lOperand * rOperand;
    this.#lOperand = result;
    return result;
  }

  sub(value) {
    let rOperand;
    if (value === undefined) {
      rOperand = this.#rOperand;
    } else {
      value = typeof value === "string" ? parseFloat(value) : value;
      if (!this.#isValidNumber(value)) throw new Error("value is not valid");
      rOperand = value;
    }

    rOperand = rOperand ?? this.#lOperand;

    const result = this.#lOperand - rOperand;
    this.#lOperand = result;
    return result;
  }

  div(value) {
    let rOperand;
    if (value === undefined) {
      rOperand = this.#rOperand;
    } else {
      value = typeof value === "string" ? parseFloat(value) : value;
      if (!this.#isValidNumber(value)) throw new Error("value is not valid");
      rOperand = value;
    }

    rOperand = rOperand ?? this.#lOperand;

    if (rOperand === 0) throw new Error("division by zero");

    const result = this.#lOperand / rOperand;
    this.#lOperand = result;
    return result;
  }
}
