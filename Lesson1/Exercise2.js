let firstValue = parseFloat(prompt("Введите первое значение", ''));

if (isNaN(firstValue)) {
    console.log("Некорректный ввод!");
  } else {
    let secondValue = parseFloat(prompt("Введите второе значение", ''));

    if (isNaN(secondValue)) {
    console.log("Некорректный ввод!");
  } else {
    let sum = firstValue + secondValue;
    let quotient = firstValue / secondValue;
    console.log(`Ответ: ${sum}, ${quotient}.`);
  }
}