let number = parseInt(prompt("Введите исходное значение", ''));
if (isNaN(number)) {
    console.log("Некорректный ввод!");
    } else {
        let base = parseInt(prompt("Введите основание системы счисления", ''));
        if (isNaN(base) || !(base >= 2 && base <=36)) {
        console.log("Некорректный ввод!");
    } else {
        let value = number.toString(base);
        console.log(value);
    }
}