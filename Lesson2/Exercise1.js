const makeObjectDeepCopy = (value) => {
    if (!(value instanceof Object)) {
        return value;
      }
    let copy;
    if (Array.isArray(value)) {
      copy = value.map((el) => makeObjectDeepCopy(el));
    } else {
      copy = {};
      for (key in value) {
        copy[key] = makeObjectDeepCopy(value[key]);
      }
    }
    return copy;
  };

const player = {
    firstName: "Cristiano",
    lastName: "Ronaldo",
    location: {
        city: "Manchester",
        club: "ManUtd",
    },
    score: [3, 1, 1, 1, 0]
};

let copy = makeObjectDeepCopy(player);

console.log("Original: ", player);
console.log("copy: ", copy);