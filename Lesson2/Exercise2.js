const arrayValues = [1, 7, -4, 12];
const firstValue = 3;
const secondValue = 9;

function selectFromInterval(arrayValues, firstValue, secondValue) {
    if (!(arrayValues instanceof Array)) 
      throw new Error("array should be passed as the first parameter");
  
    if (!arrayValues.every((el) => typeof el === "number"))
      throw new Error("array should contain numbers only");
  
    if (!(typeof firstValue === "number" && firstValue % 1 === 0)) 
      throw new Error("firstValue should be an integer");
  
    if (!(typeof secondValue === "number" && secondValue % 1 === 0))
      throw new Error("secondValue should be an integer");
  
    if (firstValue > secondValue)
      [firstValue, secondValue] = [secondValue, firstValue];
  
    firstValue--;
    secondValue--;
  
    if (firstValue < 0) {
      firstValue *= -1;
      let remainder = firstValue % arrayValues.length;
      firstValue -= remainder;
      firstValue += arrayValues.length - remainder;
    }
  
    if (secondValue < 0) {
      secondValue *= -1;
      let remainder = secondValue % arrayValues.length;
      secondValue -= remainder;
      secondValue += arrayValues.length - remainder;
    }
  
    return arrayValues.filter(
      (_, index) => index >= firstValue && index <= secondValue
    );
  }