class Calculator {
    #x;
    #y;
  
    constructor(x, y) {
      if (!this.#isValidNumber(x) || !this.#isValidNumber(y))
        throw new Error("x and y should have valid vlues");
  
      this.#x = x;
      this.#y = y;
  
      this.setX = this.setX.bind(this);
      this.setY = this.setY.bind(this);
      this.logSum = this.logSum.bind(this);
      this.logMul = this.logMul.bind(this);
      this.logSub = this.logSub.bind(this);
      this.logDiv = this.logDiv.bind(this);
    }
  
    #isValidNumber(v) {
      if (typeof v === "number" && !isNaN(v) && v !== Infinity && v !== -Infinity)
        return true;
      return false;
    }
  
    setX(x) {
      if (!this.#isValidNumber(x)) throw new Error("x should have valid value");
  
      this.#x = x;
    }
  
    setY(y) {
      if (!this.#isValidNumber(y)) throw new Error("y should have valid value");
  
      this.#y = y;
    }
  
    logSum() {
      console.log(this.#x + this.#y);
    }
  
    logMul() {
      console.log(this.#x * this.#y);
    }
  
    logSub() {
      console.log(this.#x - this.#y);
    }
  
    logDiv() {
      if (this.#y === 0) throw new Error("attempting divide by zero");
  
      console.log(this.#x / this.#y);
    }
  }
  
  const calc = new Calculator(6, 3);
  calc.logDiv();
  
  const logDiv = calc.logDiv;
  logDiv();