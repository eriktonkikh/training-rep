function concatStrings(string, separator) {
    const stringList = [];
    let readySeparator;
  
    if (typeof string !== "string") return "";
  
    stringList.push(string);
    if (typeof separator === "string") readySeparator = separator;
  
    return function recursion(string, separator) {
      if (typeof string !== "string")
        return stringList.join(readySeparator ?? "");
  
      stringList.push(string);
      if (typeof separator === "string") readySeparator = separator;
  
      return recursion;
    };
  }
  
  console.log(concatStrings("some-value")("333")(123n));