// Суть проблемы фунарга это реализация функции как первоклассного объекта.

// Проблема возникает, если тело функции ссылается на идентификаторы лексического окружения, где она была определена. 

// Есть два варианта проблемы: 
// -проблема восходящего фунарга; 
// -проблема нисходящего фунарга.

// Проблема восходящего фунарга возникает при возврате функции из некоторой функции.

// Проблема нисходящего фунарга возникает при передачи функции в качестве параметра некоторой функции.
