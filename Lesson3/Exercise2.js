function createDebounceFunction(callback, delay) {
    let isFree = true;
    let timeOut;
  
    return function () {
      if (isFree) {
        timeOut = setTimeout(() => {
          callback();
          isFree = true;
        }, delay);
  
        isFree = false;
        return;
      }
      clearTimeout(timeOut);
      timeOut = setTimeout(callback, delay);
    };
  }
  
  const start = Date.now();
  
  const debouncedFunction = createDebounceFunction(
    () => console.log(Date.now() - start),
    1000
  );
  
  debouncedFunction();
  setTimeout(debouncedFunction, 200);
  setTimeout(debouncedFunction, 400);