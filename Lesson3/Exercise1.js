Array.prototype.myFilter = function (callback, thisArg) {
    const array = this;
    const filteredArray = [];
    for (let i = 0; i < array.length; i++) {
      if (callback.call(thisArg, array[i], i, array))
        filteredArray.push(array[i]);
    }
  
    return filteredArray;
  };
  
  const modSetting = { modulus: 2 };
  
  console.log(
    "filter method result:",
    [0, 1, 2, 3, 4].filter(function (el, i, array) {
      console.log(`element: ${el}; index: ${i}; array: ${array};`);
      return el % this.modulus === 0;
    }, modSetting)
  );
  
  console.log(
    "myFilter method result:",
    [0, 1, 2, 3, 4].myFilter(function (el, i, array) {
      console.log(`element: ${el}; index: ${i}; array: ${array};`);
      return el % this.modulus === 0;
    }, modSetting)
  );